$(document).ready(function () {
	$(" .demo").slick();
});

function abrirpopup(idArt) {
	const art = document.getElementById(idArt);
	const popup = document.getElementById("contenedor-popup");
	const padre = document.getElementById("padre");

	document.getElementById("descripcion").innerHTML = "Descripcion: " + art.dataset.descripcion;
	document.getElementById("precio").innerHTML = "Precio: " + art.dataset.precio;
	document.getElementById("imagen").src = art.dataset.imagen;
	padre.classList.add("fondo-black");
	popup.style.opacity = "1";
	popup.style.visibility = "visible";
}

function cerrarPopup() {
	const popup = document.getElementById("contenedor-popup");
	const padre = document.getElementById("padre");
	padre.classList.remove("fondo-black");
	popup.style.opacity = "0";
	popup.style.visibility = "hiden";
}

// Funcion para validar datos del formulario
function validarDatos() {
	const nombre = document.formDatosPersonales.nombre.value;
	const apellido = document.formDatosPersonales.apellido.value;
	const correo = document.formDatosPersonales.email.value;
	const asunto = document.formDatosPersonales.asunto.value;
	const mensaje = document.formDatosPersonales.mensaje.value;
	var alerta = "";

	// validar nombre que tenga entre 4 y 30 caracteres como maximo
	if (nombre.length < 4 || nombre.length > 30 || apellido.length < 4 || apellido.length > 30) {
		alerta = "Nombre - Apellido (min 4 y max 30 letras)\n";
	}

	// validamos que nombre no contenga ningun numero entre 0 y 9
	for (var i = 0; i < 10; i++) {
		if (nombre.includes(i) || apellido.includes(i)) {
			// alert("Nombre - Apellido no admite numeros");
			alerta += "Nombre - Apellido (no admite numeros)\n";
			// break;
		}
	}

	// validamos que el correo sea una direccion valida
	if (!correo.endsWith("@gmail.com") && !correo.endsWith("@hotmail.com")) {
		alerta += "Email (Ingrese un email válido)\n";
	}

	// validamos que el asunto no tenga mas de 20 caracteres
	if (asunto.length > 20) {
		alerta += "Asunto (máximo 20 caracteres)\n";
	}

	// validamos longitud del mensaje, no debe ser menor a 10 ni mayor a 100
	if (mensaje.length < 10 || mensaje.length > 100) {
		alerta += "Mensaje (min 10 y max 100 caracteres)";
	}

	alert(alerta);
}